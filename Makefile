PROG=kfd-events
CFLAGS=-Wall -Wextra

.PHONY: clean

$(PROG):

clean:
	$(RM) $(PROG) *~
