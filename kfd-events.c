/*
 * Copyright 2021 Advanced Micro Devices, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <linux/kfd_ioctl.h>
#include <poll.h>
#include <limits.h>
#include <stdlib.h>
#include <stdint.h>

#define POLL_TIMEOUT -1

const char *prog;
static const char *kfd_dev_name = "/dev/kfd";

void exit_error(int res, const char *op, const char *str)
{
	fprintf(stderr, "%s: %s: %s: %s\n", prog, op, str, strerror(errno));
	exit(res);
}

void print_help(void)
{
	fprintf(stderr, "Usage: %s <args>\n", prog);
	fprintf(stderr, "Where <args> is optional, and is one of:\n");
	fprintf(stderr, "  -h or --help : Print this help\n");
	fprintf(stderr, "  --gpuid <gpuid> : Monitor events for this GPU ID.\n");
	fprintf(stderr, "For available GPU IDs see the output of these files,\n");
	fprintf(stderr, "  /sys/class/kfd/kfd/topology/nodes/*/gpu_id\n");
	exit(1);
}

void do_args(int argc, const char *argv[],
	     struct kfd_ioctl_smi_events_args *smie_arg)
{
	if (argc < 2) {
		/* From /sys/class/kfd/kfd/topology/nodes/.../gpu_id
		 * Note that 0 is the CPU id.
		 */
		smie_arg->gpuid = 0xDEAD;
	} else if (argv[1][0] != '-') {
		print_help();
	} else if (argv[1][1] == 'h') {
		print_help();
	} else if (argv[1][1] == '-') {
		if (strncmp(argv[1], "--help", 6) == 0) {
			print_help();
		} else if (strncmp(argv[1], "--gpuid", 8) == 0) {
			if (argc < 3) {
				print_help();
			} else {
				char *endp;
				typeof(smie_arg->gpuid) gpuid;

				gpuid = strtoul(argv[2], &endp, 0);
				if (endp == argv[2] || *endp != '\0') {
					fprintf(stderr,
						"%s: invalid gpuid: %s\n",
						prog, argv[2]);
					print_help();
				}
				smie_arg->gpuid = gpuid;
			}
		} else {
			print_help();
		}
	} else {
		print_help();
	}
}

int main(int argc, const char *argv[])
{
	struct kfd_ioctl_smi_events_args smie_arg;
	char *buffer;
	long buffer_size;
	struct pollfd pfda[1];
	int fd, res;

	prog = strrchr(argv[0], '/');
	if (prog)
		prog++;
	else
		prog = argv[0];

	memset(&smie_arg, 0, sizeof(smie_arg));
	do_args(argc, argv, &smie_arg);

	buffer_size = sysconf(_SC_PAGESIZE);
	buffer = malloc(buffer_size);
	if (buffer == NULL)
		exit_error(-1, "malloc", "out of memory");

	fd = open(kfd_dev_name, O_RDONLY);
	if (fd == -1)
		exit_error(fd, "open", kfd_dev_name);

	res = ioctl(fd, AMDKFD_IOC_SMI_EVENTS, &smie_arg);
	if (res == -1)
		exit_error(res, "ioctl", kfd_dev_name);
	close(fd);
	fd = -1;

	fprintf(stderr, "Waiting for gpuid:0x%X events on anonymous fd:%d...\n",
		smie_arg.gpuid, smie_arg.anon_fd);

	pfda[0].fd = smie_arg.anon_fd;
	pfda[0].events = POLLIN;
	pfda[0].revents = 0;
	for (;;) {
		res = poll(pfda, sizeof(pfda), POLL_TIMEOUT);
		if (res == -1) {
			exit_error(res, "poll", "anon fd");
		} else if (res == 0) {
			exit_error(res, "poll", "timeout?");
		} else if (pfda[0].revents & POLLIN) {
			while ((res = read(pfda[0].fd, buffer, buffer_size))) {
				if (res < 0) {
					exit_error(res, "read", "anon fd");
				} else if (res == 0) {
					break;
				} else {
					int wres;

					wres = write(STDOUT_FILENO, buffer, res);
					if (wres == -1)
						exit_error(wres, "write", "stdout");
					else if (wres < res)
						exit_error(wres, "write", "stdout: short write");
				}
		
			}
		}
	}

	close(smie_arg.anon_fd);
	free(buffer);
	return 0;
}
